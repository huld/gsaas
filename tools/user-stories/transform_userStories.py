import os
import csv
import codecs 
from dataclasses import dataclass
import json

@dataclass
class userStory:
    id : str
    name : str
    bucket: str
    epic: str
    details : str

    def toJson(self):
        d = { "id" : self.id, "bucket": self.bucket, "epic": self.epic, "description" : self.details}
        return d

    
def loadCSV(path, delimChar = ';'):     
    ''' 
    Load CSV with specified delimiter and returns list
    default separator => ';' 
    '''   
    #with codecs.open(path, encoding='utf-8') as f:
    with codecs.open(path, encoding="utf-8") as f:
        csvr = csv.reader(f, delimiter=delimChar, quoting=csv.QUOTE_ALL)
        data = [x for x in csvr]        
        return data
 
def tokenizeDescription(data):
    #skip the first line
    poi = {x[0]:x[3] for x in data[1:]}
    #iterate over all description fields containing joined definitions
    out = {}
    for id,item in poi.items():
        fields = item.split(";")
        pfields = list(map(lambda x: x.strip(),fields))
        d ={}
        currentkey = None
        # create a dictionary from each item starting with [ in a description field
        for pitem in pfields:
            if pitem.startswith("["):
                d.setdefault(pitem,[])
                currentkey = pitem
            else:
                d[currentkey].append(pitem)
       
        #process substories & pre-conditions
        keys = ('[Sub-stories]', '[Pre-conditions]')
        for key in keys:
            sublist = d[key][1:] if key == '[Sub-stories]' else d[key]
            a = iter(sublist)
            res = list(zip(a,a))
            d[key] = {k:v for k,v in res}
        #unwrap list if there is only one item
        d = {k: (v[0] if len(v) == 1 and isinstance(v,list) else v) for (k,v) in d.items()}
        out[id] = d
    return out

def assembly(raw,processed):
    ucList = []
    #skip the first line
    for line in raw[1:]:
        id, name = line[0].split(' ',1)
        obj = userStory(id, name, line[1],line[2], processed[line[0]])
        ucList.append(obj.toJson())
    outDict = {"UC" : ucList}
    return outDict

def saveJSON(data, filename):
    with open(filename,'w') as f:
        json.dump(data,f)
    print("Data saved to JSON")

if __name__ == "__main__":
     workdir = "."
     fileName = "GSAAS-OF-004_UC_Asana_extract_v2.csv"
     outname = "GSAAS-OF-004_UC.json"
     os.chdir(workdir)
     data = loadCSV(fileName,',')
     processed = tokenizeDescription(data)
     out = assembly(data,processed)
     saveJSON(out,os.path.join(workdir,outname))
     print("End of the script")
