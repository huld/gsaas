# UI - Description for Developers

This document describes the web application created using [React](https://reactjs.org/), [Material-UI](https://material-ui.com/), [Redux](https://redux.js.org/) etc.

## React components

TODO: Component tree

### General components

- ConfirmationDialog = dialog with "OK", "Cancel". Title and content of the dialog can be customized.
- DebugButton = button displayed in the top-right corner
- Title = text in bigger font


## Application state

Application state (i.e. all the data = single source of truth) is stored in one global store and managed by Redux and reducers.

### Reducers
- alerts = invoked alert messages
- alertDefs = definitions of alerts, i.e. conditions when to create and display alert
- data = data from satellites

### Actions
- TODO:

## Utilities & Helpers

### Logging

Functionality to log messages is in our **utils** module.
The standard [debug] library is internally used.

Usage:
1. Call `utils.debug("NameOfModule")` to create your own `log` function. 
2. Call `log` function to log messages to browser's console.
3. Log messages from one module have the same color.

Example:

```
import utils from "../utils";

const log = utils.debug("NameOfModule");

log("message");
log("message", data1, data2, etc);
```

2020, tomas.holub@huld.io